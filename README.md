# CSSMIN

CSSMIN is a simple C99 application to minify css files. It doesn't do anything fancy like SASS, it just removes whitespace and comments from CSS files. If you write CSS by hand, and just want a simple tool to remove comments and whitespace, you might find this helpful.

It is based on Douglas Crockford's 2002 `jsmin.c` (I have no idea where I initially got it from).

## Usage

```
cssmin < "$FULL_FILE_SRC_PATH" > "$FULL_FILE_OUTPUT_PATH" "$COPYRIGHT"
```

So for example:

```
./build/cssmin < ./testdata/pho-ui.css > ./build/pho-ui.css "Copyright 2021 Megacorp Inc"
```

## Building

Assuming you have _make_ and _gcc_ (or _clang_) installed, you should be able to just type:

```
make
```

And _cssmin_ should be created in the _dist_ directory.

You can also run `make check` if you have `cppcheck` installed
