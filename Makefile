CC = gcc
CFLAGS = --std=c99 -Wall -O0

build:
	mkdir -p build
	$(CC) $(CFLAGS) src/cssmin.c -o build/cssmin

clean:
	rm -rf build
	rm -f check_output.txt

test: clean build
	@cppcheck \
		--suppress=missingInclude \
		--inline-suppr \
		--enable=style \
		--template=gcc \
		--enable=all ./src 2> check_output.txt

check: test
	@cat check_output.txt